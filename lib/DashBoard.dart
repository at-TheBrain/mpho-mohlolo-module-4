import 'package:flutter/material.dart';
import 'button.dart';
import 'ZodiacSigns.dart';
import 'EditProfilePage.dart';
import 'MyZodiac.dart';

class DashBoard extends StatelessWidget {
  const DashBoard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('DashBoard'),
          elevation: 15,
        ),
        floatingActionButton: FloatingActionButton.large(
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: ((context) => Signs()),
                ));
          },
          child: Text('Make \n   a \nWish'),
          // backgroundColor: Colors.grey,
        ),
        body: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/Zodiac/dash.jpg'),
                  fit: BoxFit.cover)),
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              //Edit Profile

              // a row with avator and container
              //container has a column that have two text widgets like =
              // We are going to have something like 0 =

              Container(
                  padding: EdgeInsets.fromLTRB(20, 25, 20, 0),
                  child: Row(
                    children: <Widget>[
                      //an Avator next to another container

                      CircleAvatar(
                        backgroundImage: AssetImage('assets/Zodiac/ava.jpg'),
                        radius: 45.0,
                      ),

                      SizedBox(width: 36),

                      Container(
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Text>[
                              Text(
                                'Username',
                                style: TextStyle(
                                  fontSize: 39,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                ),
                              ),
                              Text(
                                'You are a Pisces',
                                style: TextStyle(
                                  fontSize: 13,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                ),
                              ),
                              Text(
                                'A Fish',
                                style: TextStyle(
                                  fontSize: 13,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                ),
                              ),
                            ]),
                      ),
                    ],
                  )),

              Divider(
                color: Colors.grey,
                indent: 40.0,
                endIndent: 40.0,
                height: 70,
              ),

              SizedBox(height: 25),

              button(
                  name: 'My Zodiac',
                  route: MyZodiac(),
                  colorful: Color.fromARGB(255, 142, 124, 192)),
              SizedBox(
                height: 50,
              ),
              //View Zodiac Sign
              button(
                  name: 'Edit Profile',
                  route: Profile(),
                  colorful: Color.fromARGB(255, 142, 124, 192)),
              SizedBox(
                height: 40,
              ),
              //My Zodiac Sign
            ],
          ),
        ));
  }
}
