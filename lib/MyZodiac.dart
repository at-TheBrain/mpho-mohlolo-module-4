import 'package:flutter/material.dart';

class MyZodiac extends StatelessWidget {
  const MyZodiac({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('My Zodiac'),
          elevation: 15,
        ),
        body: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/Zodiac/dash.jpg'),
                    fit: BoxFit.cover)),
            child: Container(
              child: Column(children: <Widget>[
                Container(
                  padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                  child: CircleAvatar(
                    backgroundImage: AssetImage('assets/Zodiac/pisces.png'),
                    radius: 45.0,
                  ),
                ),
                Container(
                  child: Text(
                    'Pisces',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 30,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  child: Column(children: <Widget>[
                    Container(
                      child: Text(
                        'Horoscope',
                        style: TextStyle(color: Colors.white, fontSize: 45),
                      ),
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                      height: 370.0,
                      child: ListView(children: const <Text>[
                        Text(
                          "Pisces (February 19 - March 20)" +
                              "Pisces, a water sign, is the last constellation of the zodiac. It's symbolized by two fish swimming in opposite directions, representing the constant division of Pisces's attention between fantasy and reality. As the final sign, Pisces has absorbed every lesson — the joys and the pains, the hopes and the fears — learned by all of the other signs. This makes these fish the most psychic, empathetic, and compassionate creatures of the astrological wheel. With such immense sensitivity, Pisces can easily become swallowed by emotions and must remember to stay grounded in the material realm (appropriately, Pisces rules the feet)." +
                              "Pisces is ruled by Neptune, the celestial body that governs creativity and dreams, and these ethereal fish adore exploring their boundless imaginations. In its more nefarious form, however, Neptune also oversees illusion and escapism. Neptunian energy is like the energy of the ocean: magical, mysterious, and often scary. When the fog is thick on the water, the horizon is obstructed and there is no differentiation between the sea and the sky. Pisces is ruled by Neptune, the celestial body that governs creativity and dreams, and these ethereal fish adore exploring their boundless imaginations. In its more nefarious form, however, Neptune also oversees illusion and escapism. Neptunian energy is like the energy of the ocean: magical, mysterious, and often scary. When the fog is thick on the water, the horizon is obstructed and there is no differentiation between the sea and the sky." +
                              "Those with this sun sign must be wary of mirages: These impressionable fish prefer wearing rose-colored glasses to addressing problems, which can earn Pisces a reputation for being flaky or delusional. This water sign should remember that problems can't be solved by swimming away. Willful ignorance never makes conflict disappear: It only gives it the chance to grow." +
                              "A mutable sign, Pisces effortlessly adapts to their surroundings. These visionary fish have unparalleled access to the collective unconscious through their clairvoyance and make incredible artists and creatives. Kind and gentle, they're invigorated by shared experiences of music and romance. Any relationship with mystical Pisces is guaranteed to involve deep spiritual exploration.",
                          style: TextStyle(color: Colors.white, fontSize: 16),
                        )
                      ]),
                    )
                  ]),
                ),
              ]),
            )));
  }
}

  //LAYOUT!!!

  // A CLOCK COUNTING FOR NEXT ZODIAC TO APPEAR
  //PUT A DIVITER
  // PREVIOUS MESSAGES IN TEXT.

