import 'package:flutter/material.dart';
import 'package:universe/DashBoard.dart';
import 'TextField.dart';
import 'button.dart';

class Registration extends StatelessWidget {
  const Registration({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.deepOrangeAccent,
          title: const Text(
            'Welcome on Board Gazor :)',
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 24,
                color: Colors.white,
                fontFamily: 'IndieFlower'),
          ),
        ),
        body: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/Zodiac/try1.gif'),
                  fit: BoxFit.cover)),
          child: Center(
            child: Container(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    //Space Betweeen
                    SizedBox(height: 52),

                    //Good Feel Text
                    Text(
                      'Please fill in your details',
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.white,
                      ),
                    ),

                    //Space Between
                    SizedBox(height: 45),

                    //username
                    TextFClass(obscureText: false, textDescript: "Username"),
                    SizedBox(height: 15),
                    //Email Address
                    TextFClass(
                        obscureText: false, textDescript: "Email Address"),
                    SizedBox(height: 15),

                    //Password
                    TextFClass(obscureText: false, textDescript: "Password"),
                    SizedBox(height: 15),

                    //Confirm Pass
                    TextFClass(
                        obscureText: false, textDescript: "Confirm Password"),
                    SizedBox(height: 15),

                    //Date of Birth
                    TextFClass(
                        obscureText: false, textDescript: "Date of Birth"),
                    SizedBox(height: 15),

                    //Register Button
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 25.0),
                      child: Container(
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                            color: Colors.deepOrangeAccent,
                            borderRadius: BorderRadius.circular(12)),
                        child: Center(
                          child: button(
                            colorful: Colors.deepOrangeAccent,
                            name: 'Register',
                            route: DashBoard(),
                          ),
                        ),
                      ),
                    ),
                  ]),
            ),
          ),
        ));
  }
}
