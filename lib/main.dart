// ignore_for_file: avoid_print

import 'package:flutter/material.dart';
import 'LoginPage.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';

void main() {
  Function duringSplash = () {
    print('Something background process');
    int a = 123 + 23;
    print(a);

    if (a > 100)
      return 1;
    else
      return 2;
  };
  Map<int, Widget> op = {1: MyApp(), 2: LoginPage()};

  runApp(MaterialApp(
    home: AnimatedSplashScreen(
      // backgroundColor: Colors.blueGrey,
      splash: Image.asset(
        'assets/Zodiac/splash.jpg',
      ),
      nextScreen: LoginPage(),
      // animationDuration: duringSplash(),
      duration: 2500,
      splashIconSize: 110,
    ),
  ));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: LoginPage(),
      theme: ThemeData(
        floatingActionButtonTheme:
            FloatingActionButtonThemeData(backgroundColor: Colors.blueGrey),
        appBarTheme: AppBarTheme(color: Colors.blueGrey),
        fontFamily: 'indie-flower',
        buttonColor: Colors.deepPurpleAccent,
      ),
    );
  }
}
